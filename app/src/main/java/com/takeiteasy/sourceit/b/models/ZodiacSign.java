package com.takeiteasy.sourceit.b.models;

public class ZodiacSign {
    private final String signName;
    private final int startAtDayOfYear;
    private final int endAtDayOfYear;

    public ZodiacSign(String signName, int startAtDayOfYear, int endAtDayOfYear) {
        this.signName = signName;
        this.startAtDayOfYear = startAtDayOfYear;
        this.endAtDayOfYear = endAtDayOfYear;
    }

    public String getSignName() {
        return signName;
    }

    public boolean isInRange(int dayOfYear) {
        return dayOfYear >= startAtDayOfYear && dayOfYear <= endAtDayOfYear;
    }
}
