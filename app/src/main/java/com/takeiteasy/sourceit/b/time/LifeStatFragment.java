package com.takeiteasy.sourceit.b.time;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.takeiteasy.sourceit.R;
import com.takeiteasy.sourceit.b.models.UserLifeStat;

public class LifeStatFragment extends Fragment {
    private static final String ARG_LIFE_STAT = "arg_life_stat";

    private TextView tvUserName;
    private TextView tvYearsOld;
    private TextView tvDaysOld;
    private TextView tvSecondsOld;
    private TextView tvZodiac;

    private UserLifeStat lifeStat;

    public LifeStatFragment() {
        // Required empty public constructor
    }


    public static LifeStatFragment newInstance(UserLifeStat lifeStat) {
        LifeStatFragment fragment = new LifeStatFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_LIFE_STAT, lifeStat);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            lifeStat = getArguments().getParcelable(ARG_LIFE_STAT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_life_stat, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        showLifeStat();
    }

    private void showLifeStat() {
        tvUserName.setText(lifeStat.getName());
        tvYearsOld.setText(getString(R.string.years_old_pattern, lifeStat.getYearsOld()));
        tvDaysOld.setText(getString(R.string.days_old_pattern, lifeStat.getDaysOld()));
        tvSecondsOld.setText(getString(R.string.seconds_old_pattern, lifeStat.getSecondsOld()));
        tvZodiac.setText(getString(R.string.zodiac_pattern, lifeStat.getZodiac()));
    }

    private void initViews(View view) {
        tvUserName = view.findViewById(R.id.tv_name);
        tvYearsOld = view.findViewById(R.id.tv_years_old);
        tvDaysOld = view.findViewById(R.id.tv_days_old);
        tvSecondsOld = view.findViewById(R.id.tv_seconds_old);
        tvZodiac = view.findViewById(R.id.tv_zodiac);
    }
}
