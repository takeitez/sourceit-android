package com.takeiteasy.sourceit.b.notations;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.takeiteasy.sourceit.R;

public class NumberNotationsFragment extends Fragment {

    private EditText etFirstNumber;
    private EditText etSecondNumber;
    private EditText etThirdNumber;
    private TextView tvBinResult;
    private TextView tvOctResult;
    private TextView tvHexResult;

    private TextWatcher firstNumberTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            tvBinResult.setText(Integer.toBinaryString(parseInteger(editable.toString())));
        }
    };

    private TextWatcher secondNumberTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            tvOctResult.setText(Integer.toOctalString(parseInteger(editable.toString())));
        }
    };

    private TextWatcher thirdNumberTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            tvHexResult.setText(Integer.toHexString(parseInteger(editable.toString())));
        }
    };

    public NumberNotationsFragment() {
        // Required empty public constructor
    }

    public static NumberNotationsFragment newInstance() {
        Bundle args = new Bundle();
        NumberNotationsFragment fragment = new NumberNotationsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_number_notations, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        etFirstNumber = view.findViewById(R.id.et_first_number);
        etSecondNumber = view.findViewById(R.id.et_second_number);
        etThirdNumber = view.findViewById(R.id.et_third_number);
        tvBinResult = view.findViewById(R.id.tv_bin_result);
        tvOctResult = view.findViewById(R.id.tv_oct_result);
        tvHexResult = view.findViewById(R.id.tv_hex_result);

        etFirstNumber.addTextChangedListener(firstNumberTextWatcher);
        etSecondNumber.addTextChangedListener(secondNumberTextWatcher);
        etThirdNumber.addTextChangedListener(thirdNumberTextWatcher);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        etFirstNumber.removeTextChangedListener(firstNumberTextWatcher);
        etSecondNumber.removeTextChangedListener(secondNumberTextWatcher);
        etThirdNumber.removeTextChangedListener(thirdNumberTextWatcher);
    }

    private int parseInteger(String number) {
        if (TextUtils.isEmpty(number)) {
            return 0;
        }

        return Integer.valueOf(number);
    }
}
