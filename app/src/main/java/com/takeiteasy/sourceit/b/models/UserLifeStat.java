package com.takeiteasy.sourceit.b.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class UserLifeStat implements Parcelable {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());

    public static final int ERROR_INVALID_NAME = 0x10;
    public static final int ERROR_INVALID_DATE = 0x11;
    private static final String UNKNOWN = "UNKNOWN";

    private String name;
    private Calendar dayOfBirth;

    public UserLifeStat(String name, String lifeTime) {
        this.name = name;
        try {
            Date date = parseDate(lifeTime);
            this.dayOfBirth = Calendar.getInstance();
            this.dayOfBirth.setTime(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private Date parseDate(String date) throws ParseException {
        return DATE_FORMAT.parse(date);
    }

    public String getName() {
        return name;
    }

    public int getYearsOld() {
        Calendar today = Calendar.getInstance();

        int age = getYearsBetween(today, dayOfBirth);

        if (!canCountLastYear(today, dayOfBirth))
            age--;

        return age;
    }

    private int getYearsBetween(Calendar first, Calendar second) {
        return first.get(Calendar.YEAR) - second.get(Calendar.YEAR);
    }

    private boolean canCountLastYear(Calendar first, Calendar second) {
        return first.get(Calendar.DAY_OF_YEAR) >= second.get(Calendar.DAY_OF_YEAR);
    }

    public long getDaysOld() {
        return getDaysBetween(Calendar.getInstance(), dayOfBirth);
    }

    private long getDaysBetween(Calendar first, Calendar second) {
        return TimeUnit.MILLISECONDS.toDays(first.getTimeInMillis() - second.getTimeInMillis());
    }

    public long getSecondsOld() {
        return getSecondsBetween(Calendar.getInstance(), dayOfBirth);
    }

    private long getSecondsBetween(Calendar first, Calendar second) {
        return TimeUnit.MILLISECONDS.toSeconds(first.getTimeInMillis() - second.getTimeInMillis());
    }

    public String getZodiac() {
        ZodiacSign sign = ZodiacCalendar.getInstance().getZodiacSignForDate(dayOfBirth);

        if (sign != null)
            return sign.getSignName();

        return UNKNOWN;
    }

    public void validate(ValidationCallback validationCallback) {
        if (!isValidName()) {
            validationCallback.validationError(ERROR_INVALID_NAME);
            return;
        }

        if (!isValidDate()) {
            validationCallback.validationError(ERROR_INVALID_DATE);
            return;
        }

        validationCallback.validationSuccess();
    }

    private boolean isValidDate() {
        return dayOfBirth != null && dayOfBirth.before(Calendar.getInstance());
    }

    private boolean isValidName() {
        return !TextUtils.isEmpty(name);
    }

    public interface ValidationCallback {
        void validationSuccess();
        void validationError(int error);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeSerializable(this.dayOfBirth);
    }

    protected UserLifeStat(Parcel in) {
        this.name = in.readString();
        this.dayOfBirth = (Calendar) in.readSerializable();
    }

    public static final Parcelable.Creator<UserLifeStat> CREATOR = new Parcelable.Creator<UserLifeStat>() {
        @Override
        public UserLifeStat createFromParcel(Parcel source) {
            return new UserLifeStat(source);
        }

        @Override
        public UserLifeStat[] newArray(int size) {
            return new UserLifeStat[size];
        }
    };
}
