package com.takeiteasy.sourceit.b.navigator;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.takeiteasy.sourceit.R;

public class BSubTasksNavigationFragment extends Fragment {

    private Button btnTimeInput;
    private Button btnColorChanger;
    private Button btnNumbersNotation;

    private OnSubTaskChosenListener mListener;

    public BSubTasksNavigationFragment() {
        // Required empty public constructor
    }

    public static BSubTasksNavigationFragment newInstance() {
        Bundle args = new Bundle();
        BSubTasksNavigationFragment fragment = new BSubTasksNavigationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bsubtasks_navigation, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
    }

    private void initViews(View view) {
        btnTimeInput = view.findViewById(R.id.btn_time_input);
        btnColorChanger = view.findViewById(R.id.btn_color_changer);
        btnNumbersNotation = view.findViewById(R.id.btn_numbers_notation);

        btnTimeInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.showTimeInput(R.string.time_input);
            }
        });

        btnColorChanger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.showColorChanger(R.string.color_changer);
            }
        });

        btnNumbersNotation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.showNumbersNotation(R.string.numbers_notation);
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnSubTaskChosenListener) {
            mListener = (OnSubTaskChosenListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnSubTaskChosenListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnSubTaskChosenListener {
        void showTimeInput(@StringRes int titleRes);
        void showColorChanger(@StringRes int titleRes);
        void showNumbersNotation(@StringRes int titleRes);
    }
}
