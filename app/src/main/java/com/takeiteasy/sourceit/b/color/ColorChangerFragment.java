package com.takeiteasy.sourceit.b.color;


import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.takeiteasy.sourceit.R;

public class ColorChangerFragment extends Fragment {

    @ColorRes
    private static final int[] colorResources = new int[] {
            R.color.colorFirst, R.color.colorSecond, R.color.colorThird, R.color.colorFourth, R.color.colorFifth,
            R.color.colorSixth, R.color.colorSeventh, R.color.colorEighth, R.color.colorNinth, R.color.colorTenth
    };

    private View vFirst;
    private View vSecond;
    private View vThird;
    private View vFourth;

    private int[] lastAppliedColorResources = new int[4];

    public ColorChangerFragment() {
        // Required empty public constructor
    }

    public static ColorChangerFragment newInstance() {
        Bundle args = new Bundle();
        ColorChangerFragment fragment = new ColorChangerFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_color_changer, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);

        changeFirstViewColor();
        changeSecondViewColor();
        changeThirdViewColor();
        changeFourthViewColor();
    }

    private void initViews(View view) {
        vFirst = view.findViewById(R.id.v_first);
        vSecond = view.findViewById(R.id.v_second);
        vThird = view.findViewById(R.id.v_third);
        vFourth = view.findViewById(R.id.v_fourth);

        vFirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeSecondViewColor();
                changeThirdViewColor();
                changeFourthViewColor();
            }
        });

        vSecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeFirstViewColor();
                changeThirdViewColor();
                changeFourthViewColor();
            }
        });

        vThird.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeFirstViewColor();
                changeSecondViewColor();
                changeFourthViewColor();
            }
        });

        vFourth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeFirstViewColor();
                changeSecondViewColor();
                changeThirdViewColor();
            }
        });
    }

    private void changeFirstViewColor() {
        @ColorRes int color = getUniqueColorRes();
        setViewColor(vFirst, color);
        lastAppliedColorResources[0] = color;
    }

    private void changeSecondViewColor() {
        @ColorRes int color = getUniqueColorRes();
        setViewColor(vSecond, color);
        lastAppliedColorResources[1] = color;
    }

    private void changeThirdViewColor() {
        @ColorRes int color = getUniqueColorRes();
        setViewColor(vThird, color);
        lastAppliedColorResources[2] = color;
    }

    private void changeFourthViewColor() {
        @ColorRes int color = getUniqueColorRes();
        setViewColor(vFourth, color);
        lastAppliedColorResources[3] = color;
    }

    private void setViewColor(View view, @ColorRes int colorRes) {
        view.setBackgroundColor(ContextCompat.getColor(getContext(), colorRes));
    }

    @ColorRes
    private int getUniqueColorRes() {
        for (@ColorRes int colorRes : colorResources) {
            if (!isColorAlreadyApplied(colorRes)) {
                return colorRes;
            }
        }

        return colorResources[0];
    }

    private boolean isColorAlreadyApplied(@ColorRes int colorRes) {
        for (@ColorRes int res : lastAppliedColorResources) {
            if (res == colorRes)
                return true;
        }

        return false;
    }
}
