package com.takeiteasy.sourceit.b.models;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ZodiacCalendar {
    private static final ZodiacCalendar instance = new ZodiacCalendar();

    public static ZodiacCalendar getInstance() {
        return instance;
    }

    private final List<ZodiacSign> zodiacSigns;

    private ZodiacCalendar() {
        zodiacSigns = createZodiacSignsList();
    }

    public ZodiacSign getZodiacSignForDate(Calendar calendar) {
        int dayOfYear = calendar.get(Calendar.DAY_OF_YEAR);

        for (ZodiacSign sign : zodiacSigns) {
            if (sign.isInRange(dayOfYear))
                return sign;
        }

        return null;
    }

    private List<ZodiacSign> createZodiacSignsList() {
        List<ZodiacSign> list = new ArrayList<>();

        list.add(new ZodiacSign("Capricorn", 356, 19));
        list.add(new ZodiacSign("Aquarius", 20, 49));
        list.add(new ZodiacSign("Pisces", 50, 79));
        list.add(new ZodiacSign("Aries", 80, 109));
        list.add(new ZodiacSign("Taurus", 110, 140));
        list.add(new ZodiacSign("Gemini", 141, 171));
        list.add(new ZodiacSign("Cancer", 172, 203));
        list.add(new ZodiacSign("Leo", 204, 234));
        list.add(new ZodiacSign("Virgo", 235, 265));
        list.add(new ZodiacSign("Libra", 266, 295));
        list.add(new ZodiacSign("Scorpio", 296, 325));
        list.add(new ZodiacSign("Sagittarius", 326, 355));

        return list;
    }
}
