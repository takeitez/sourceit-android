package com.takeiteasy.sourceit.b.time;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.takeiteasy.sourceit.R;
import com.takeiteasy.sourceit.b.models.UserLifeStat;

public class TimeInputFragment extends Fragment {

    private EditText etName;
    private EditText etDate;
    private Button btnNext;

    private OnTimeSetListener mListener;

    public TimeInputFragment() {
        // Required empty public constructor
    }

    public static TimeInputFragment newInstance() {
        Bundle args = new Bundle();
        TimeInputFragment fragment = new TimeInputFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_time_input, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
    }

    private void initViews(View view) {
        etName = view.findViewById(R.id.et_name);
        etDate = view.findViewById(R.id.et_date);
        btnNext = view.findViewById(R.id.btn_next);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               next();
            }
        });
    }

    private void next() {
        final UserLifeStat lifeStat = new UserLifeStat(etName.getText().toString(), etDate.getText().toString());

        lifeStat.validate(new UserLifeStat.ValidationCallback() {
            @Override
            public void validationSuccess() {
                mListener.showLifeStat(lifeStat);
            }

            @Override
            public void validationError(int error) {
                if (error == UserLifeStat.ERROR_INVALID_NAME) {
                    showError(R.string.missing_field);
                } else if (error == UserLifeStat.ERROR_INVALID_DATE) {
                    showError(R.string.invalid_date);
                }
            }
        });
    }

    private void showError(@StringRes int errorRes) {
        Toast.makeText(getContext(), errorRes, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnTimeSetListener) {
            mListener = (OnTimeSetListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnTimeSetListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnTimeSetListener {
        void showLifeStat(UserLifeStat lifeStat);
    }
}
