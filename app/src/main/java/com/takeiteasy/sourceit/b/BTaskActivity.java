package com.takeiteasy.sourceit.b;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.takeiteasy.sourceit.R;
import com.takeiteasy.sourceit.b.color.ColorChangerFragment;
import com.takeiteasy.sourceit.b.models.UserLifeStat;
import com.takeiteasy.sourceit.b.navigator.BSubTasksNavigationFragment;
import com.takeiteasy.sourceit.b.notations.NumberNotationsFragment;
import com.takeiteasy.sourceit.b.time.LifeStatFragment;
import com.takeiteasy.sourceit.b.time.TimeInputFragment;

public class BTaskActivity extends AppCompatActivity
        implements FragmentManager.OnBackStackChangedListener, BSubTasksNavigationFragment.OnSubTaskChosenListener,
        TimeInputFragment.OnTimeSetListener {

    private Toolbar toolbar;

    @Override
    protected void onStart() {
        super.onStart();
        getSupportFragmentManager().addOnBackStackChangedListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        getSupportFragmentManager().removeOnBackStackChangedListener(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_btask);
        setupActionBar();

        setFragment(BSubTasksNavigationFragment.newInstance());
    }
    private void setupActionBar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void setFragment(Fragment fragment) {
        setFragment(fragment, false);
    }

    private void setFragment(Fragment fragment, boolean addToBackStack) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        ft.replace(R.id.container, fragment);

        if (addToBackStack)
            ft.addToBackStack(null);

        ft.commit();
    }

    @Override
    public void onBackStackChanged() {
        ActionBar actionBar = getSupportActionBar();

        if (actionBar == null)
            return;

        boolean isBackStackEmpty = getSupportFragmentManager().getBackStackEntryCount() == 0;

        actionBar.setDisplayHomeAsUpEnabled(!isBackStackEmpty);

        if (isBackStackEmpty)
            setTitle(R.string.task_b);
    }

    @Override
    public void showTimeInput(int titleRes) {
        setFragment(TimeInputFragment.newInstance(), true);
        setTitle(titleRes);
    }

    @Override
    public void showColorChanger(int titleRes) {
        setFragment(ColorChangerFragment.newInstance(), true);
        setTitle(titleRes);
    }

    @Override
    public void showNumbersNotation(int titleRes) {
        setFragment(NumberNotationsFragment.newInstance(), true);
        setTitle(titleRes);
    }

    @Override
    public void showLifeStat(UserLifeStat lifeStat) {
        setFragment(LifeStatFragment.newInstance(lifeStat), true);
    }
}
