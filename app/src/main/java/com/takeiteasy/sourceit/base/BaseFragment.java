package com.takeiteasy.sourceit.base;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.takeiteasy.sourceit.R;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public  class BaseFragment extends Fragment {

    private Unbinder unbinder;

    public BaseFragment() {
        // Required empty public constructor
    }

    protected View bindView(View view) {
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }
}
