package com.takeiteasy.sourceit.e.commodity;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;

import com.takeiteasy.sourceit.e.models.Commodity;
import com.takeiteasy.sourceit.e.models.Store;

import java.util.Random;

public class CommodityCirculationHandlerThread extends HandlerThread {
    private static final String COMMODITY_CIRCULATION_THREAD = "COMMODITY_CIRCULATION_THREAD";
    private static final int EVENT_DELAY_IN_MILLIS = 1_000;
    private static final int CMD_RUN = 0x10;

    private Handler backgroundHandler;
    private Handler uiHandler;
    private Store store;
    private Runnable notifyStoreUpdated = new Runnable() {
        @Override
        public void run() {
            store.notifyItemsUpdated();
        }
    };

    public CommodityCirculationHandlerThread(Handler uiHandler, Store store) {
        super(COMMODITY_CIRCULATION_THREAD);
        this.uiHandler = uiHandler;
        this.store = store;
    }

    @Override
    protected void onLooperPrepared() {
        backgroundHandler = new Handler(getLooper(), new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                int cmd = msg.what;

                if (cmd == CMD_RUN) {

                    buyRandomCommodityNumber();

                    requestNextEvent();
                }

                return true;
            }
        });

        backgroundHandler.sendEmptyMessage(CMD_RUN);
    }

    private void requestNextEvent() {
        backgroundHandler.sendEmptyMessageDelayed(CMD_RUN, EVENT_DELAY_IN_MILLIS);
    }

    private void buyRandomCommodityNumber() {
        Random random = new Random();

        for (Commodity commodity : store.getAvailableCommodities()) {
            int amount = random.nextInt(10);

            if (store.isWorking()) {
                store.buy(commodity, amount);
                uiHandler.post(notifyStoreUpdated);
            } else {
                store.putOrderInQueue(commodity, amount);
            }
        }
    }

    @Override
    public boolean quit() {
        store = null;
        return super.quit();
    }
}
