package com.takeiteasy.sourceit.e.models;

import android.support.annotation.Nullable;

import com.takeiteasy.sourceit.e.utils.DataGenerator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

public class Store {
    private final List<Commodity> commodities;
    private final Map<Commodity, Integer> commoditiesCirculation;

    private Queue<CommodityCirculationNode> queue;
    private boolean isWorking;
    private ItemsUpdateListener listener;


    public Store() {
        commodities = DataGenerator.createCommoditiesList();
        commoditiesCirculation = new HashMap<>();
        queue = new LinkedList<>();
        isWorking = true;
    }

    public void setListener(@Nullable ItemsUpdateListener listener) {
        this.listener = listener;
    }

    public void setWorking(boolean working) {
        if (!isWorking && working) {
            processQueue();
        }

        isWorking = working;
    }

    private void processQueue() {
        while (!queue.isEmpty()) {
            CommodityCirculationNode item = queue.remove();
            buy(item.getCommodity(), item.getAmount());
        }
    }

    public boolean isWorking() {
        return isWorking;
    }

    public void buy(Commodity commodity, int amount) {
        if (commoditiesCirculation.containsKey(commodity)) {
            commoditiesCirculation.put(commodity, commoditiesCirculation.get(commodity) + amount);
        } else {
            commoditiesCirculation.put(commodity, amount);
        }
    }

    public void putOrderInQueue(Commodity commodity, int amount) {
        queue.add(new CommodityCirculationNode(commodity, amount));
    }

    public void notifyItemsUpdated() {
        if (listener != null) {
            listener.onItemsUpdated(getItemsList());
        }
    }

    private List<CommodityCirculationNode> getItemsList() {
        List<CommodityCirculationNode> list = new ArrayList<>();

        for (Map.Entry<Commodity, Integer> entry : commoditiesCirculation.entrySet()) {
            list.add(new CommodityCirculationNode(entry.getKey(), entry.getValue()));
        }

        return list;
    }

    public List<Commodity> getAvailableCommodities() {
        return commodities;
    }

    public interface ItemsUpdateListener {
        void onItemsUpdated(List<CommodityCirculationNode> items);
    }
}
