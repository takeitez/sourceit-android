package com.takeiteasy.sourceit.e.commodity;

import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.takeiteasy.sourceit.R;
import com.takeiteasy.sourceit.e.models.CommodityCirculationNode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CommoditiesCirculationRecyclerViewAdapter extends RecyclerView.Adapter<CommoditiesCirculationRecyclerViewAdapter.ViewHolder> {

    private final List<CommodityCirculationNode> data;

    public CommoditiesCirculationRecyclerViewAdapter() {
        data = new ArrayList<>();
    }

    public void updateData(List<CommodityCirculationNode> data) {
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public CommoditiesCirculationRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(inflateView(parent, R.layout.item_commodity));
    }

    private View inflateView(ViewGroup parent, @LayoutRes int layoutRes) {
        return LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
    }

    @Override
    public void onBindViewHolder(CommoditiesCirculationRecyclerViewAdapter.ViewHolder holder, int position) {
        holder.bindData(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_number)
        TextView number;
        @BindView(R.id.tv_name)
        TextView name;
        @BindView(R.id.tv_amount)
        TextView amount;
        @BindView(R.id.tv_total)
        TextView total;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bindData(CommodityCirculationNode node) {
            number.setText(String.valueOf(getAdapterPosition() + 1));
            name.setText(node.getCommodityName());
            amount.setText(itemView.getContext().getString(R.string.amount_pattern, node.getAmount()));
            total.setText(itemView.getContext().getString(R.string.total_cost_pattern, node.getTotalCost()));
        }
    }
}
