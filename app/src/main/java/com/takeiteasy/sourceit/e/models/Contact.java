package com.takeiteasy.sourceit.e.models;

import android.net.Uri;

import java.io.Serializable;

public class Contact implements Serializable {
    private String name;
    private String email;
    private String address;
    private Uri icon;

    public Contact(String name, String email, String address, Uri icon) {
        this.name = name;
        this.email = email;
        this.address = address;
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public Uri getIcon() {
        return icon;
    }
}
