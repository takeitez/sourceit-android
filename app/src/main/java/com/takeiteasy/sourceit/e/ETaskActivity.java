package com.takeiteasy.sourceit.e;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.takeiteasy.sourceit.R;
import com.takeiteasy.sourceit.e.commodity.CommodityCirculationFragment;
import com.takeiteasy.sourceit.e.contacts.ContactsFragment;
import com.takeiteasy.sourceit.e.details.ContactDetailsFragment;
import com.takeiteasy.sourceit.e.models.Contact;
import com.takeiteasy.sourceit.e.navigator.ESubTasksNavigationFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ETaskActivity extends AppCompatActivity
        implements FragmentManager.OnBackStackChangedListener, ESubTasksNavigationFragment.OnSubTaskChosenListener,
        ContactsFragment.ContactsInteractionListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_etask);
        ButterKnife.bind(this);
        setupActionBar();

        setFragment(ESubTasksNavigationFragment.newInstance());

        getSupportFragmentManager().addOnBackStackChangedListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getSupportFragmentManager().removeOnBackStackChangedListener(this);
    }

    private void setupActionBar() {
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void setFragment(Fragment fragment) {
        setFragment(fragment, false);
    }

    private void setFragment(Fragment fragment, boolean addToBackStack) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        ft.replace(R.id.container, fragment);

        if (addToBackStack)
            ft.addToBackStack(null);

        ft.commit();
    }

    @Override
    public void onBackStackChanged() {
        ActionBar actionBar = getSupportActionBar();

        if (actionBar == null)
            return;

        boolean isBackStackEmpty = getSupportFragmentManager().getBackStackEntryCount() == 0;

        actionBar.setDisplayHomeAsUpEnabled(!isBackStackEmpty);

        if (isBackStackEmpty)
            setTitle(R.string.task_e);
    }

    @Override
    public void showContactList(int titleRes) {
        setFragment(ContactsFragment.newInstance(), true);
        setTitle(titleRes);
    }

    @Override
    public void showCommodityCirculation(int titleRes) {
        setFragment(CommodityCirculationFragment.newInstance(), true);
        setTitle(titleRes);
    }

    @Override
    public void showContactDetails(Contact contact) {
        setFragment(ContactDetailsFragment.newInstance(contact), true);
    }
}
