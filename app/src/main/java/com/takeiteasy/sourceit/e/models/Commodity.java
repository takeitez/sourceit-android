package com.takeiteasy.sourceit.e.models;

public class Commodity {
    private String name;
    private float cost;

    public Commodity(String name, float cost) {
        this.name = name;
        this.cost = cost;
    }

    public String getName() {
        return name;
    }

    public float getCost() {
        return cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Commodity commodity = (Commodity) o;

        if (Float.compare(commodity.cost, cost) != 0) return false;
        return name.equals(commodity.name);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + (cost != +0.0f ? Float.floatToIntBits(cost) : 0);
        return result;
    }
}
