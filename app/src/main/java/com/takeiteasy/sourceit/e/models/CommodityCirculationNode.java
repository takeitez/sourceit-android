package com.takeiteasy.sourceit.e.models;

public class CommodityCirculationNode {
    private Commodity commodity;
    private int amount;

    public CommodityCirculationNode(Commodity commodity, int amount) {
        this.commodity = commodity;
        this.amount = amount;
    }

    public Commodity getCommodity() {
        return commodity;
    }

    public String getCommodityName() {
        return commodity.getName();
    }

    public float getTotalCost() {
        return amount * commodity.getCost();
    }

    public int getAmount() {
        return amount;
    }
}
