package com.takeiteasy.sourceit.e.commodity;


import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.takeiteasy.sourceit.R;
import com.takeiteasy.sourceit.base.BaseFragment;
import com.takeiteasy.sourceit.e.models.CommodityCirculationNode;
import com.takeiteasy.sourceit.e.models.Store;

import java.util.List;

import butterknife.BindView;

public class CommodityCirculationFragment extends BaseFragment implements Store.ItemsUpdateListener {

    @BindView(R.id.list)
//    ListView list;
    RecyclerView list;

    private Store store;
//    private CommoditiesCirculationListViewAdapter adapter;
    private CommoditiesCirculationRecyclerViewAdapter adapter;
    private CommodityCirculationHandlerThread handlerThread;

    public static CommodityCirculationFragment newInstance() {

        Bundle args = new Bundle();

        CommodityCirculationFragment fragment = new CommodityCirculationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_work_trigger, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.work_trigger) {

            store.setWorking(!store.isWorking());
            item.setTitle(store.isWorking() ? R.string.stop : R.string.start);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        return bindView(inflater.inflate(R.layout.fragment_commodity_circulation_list_view, container, false));
        return bindView(inflater.inflate(R.layout.fragment_commodity_circulation_recycler_view, container, false));
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        store = new Store();
        store.setListener(this);

//        adapter = new CommoditiesCirculationListViewAdapter();
        adapter = new CommoditiesCirculationRecyclerViewAdapter();

        list.setLayoutManager(new LinearLayoutManager(getContext()));
        list.addItemDecoration(new DividerItemDecoration(view.getContext(), DividerItemDecoration.VERTICAL));
        list.setAdapter(adapter);

        handlerThread = new CommodityCirculationHandlerThread(new Handler(), store);
        handlerThread.start();
    }

    @Override
    public void onDestroyView() {
        store.setListener(null);
        handlerThread.quit();
        super.onDestroyView();
    }

    @Override
    public void onItemsUpdated(List<CommodityCirculationNode> items) {
        adapter.updateData(items);
    }
}
