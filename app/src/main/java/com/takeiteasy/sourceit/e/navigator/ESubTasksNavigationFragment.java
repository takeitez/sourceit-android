package com.takeiteasy.sourceit.e.navigator;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.takeiteasy.sourceit.R;
import com.takeiteasy.sourceit.base.BaseFragment;

import butterknife.OnClick;

public class ESubTasksNavigationFragment extends BaseFragment {

    private OnSubTaskChosenListener mListener;

    public static ESubTasksNavigationFragment newInstance() {

        Bundle args = new Bundle();

        ESubTasksNavigationFragment fragment = new ESubTasksNavigationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return bindView(inflater.inflate(R.layout.fragment_navigator, container, false));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnSubTaskChosenListener) {
            mListener = (OnSubTaskChosenListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnSubTaskChosenListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick(R.id.btn_contacts)
    void showContacts() {
        mListener.showContactList(R.string.contacts);
    }

    @OnClick(R.id.btn_commodity_circulation)
    void showCommodityCirculation() {
        mListener.showCommodityCirculation(R.string.commodity_circulation);
    }

    public interface OnSubTaskChosenListener {
        void showContactList(@StringRes int titleRes);
        void showCommodityCirculation(@StringRes int titleRes);
    }
}
