package com.takeiteasy.sourceit.e.details;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.takeiteasy.sourceit.R;
import com.takeiteasy.sourceit.base.BaseFragment;
import com.takeiteasy.sourceit.e.models.Contact;

import butterknife.BindView;

public class ContactDetailsFragment extends BaseFragment {
    private static final String ARG_CONTACT = "arg_contact";

    @BindView(R.id.iv_icon)
    ImageView icon;
    @BindView(R.id.tv_name)
    TextView name;
    @BindView(R.id.tv_email)
    TextView email;
    @BindView(R.id.tv_address)
    TextView address;

    private Contact contact;

    public static ContactDetailsFragment newInstance(Contact contact) {

        Bundle args = new Bundle();
        args.putSerializable(ARG_CONTACT, contact);
        ContactDetailsFragment fragment = new ContactDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            contact = (Contact) getArguments().getSerializable(ARG_CONTACT);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (contact.getIcon() != null) {
            icon.setImageURI(contact.getIcon());
        }

        name.setText(contact.getName());
        email.setText(contact.getEmail());
        address.setText(contact.getAddress());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return bindView(inflater.inflate(R.layout.fragment_contact_details, container, false));
    }

}
