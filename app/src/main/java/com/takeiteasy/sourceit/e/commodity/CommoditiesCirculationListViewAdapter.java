package com.takeiteasy.sourceit.e.commodity;

import android.support.annotation.LayoutRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.takeiteasy.sourceit.R;
import com.takeiteasy.sourceit.e.models.CommodityCirculationNode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CommoditiesCirculationListViewAdapter extends BaseAdapter {

    private final List<CommodityCirculationNode> data;

    public CommoditiesCirculationListViewAdapter() {
        data = new ArrayList<>();
    }

    public void updateData(List<CommodityCirculationNode> data) {
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public CommodityCirculationNode getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder viewHolder;

        if (convertView == null) {
            view = inflateView(parent, R.layout.item_commodity);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            view = convertView;
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.bindData(getItem(position), position);

        return view;
    }

    private View inflateView(ViewGroup parent, @LayoutRes int layoutRes) {
        return LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
    }

    class ViewHolder {
        @BindView(R.id.tv_number)
        TextView number;
        @BindView(R.id.tv_name)
        TextView name;
        @BindView(R.id.tv_amount)
        TextView amount;
        @BindView(R.id.tv_total)
        TextView total;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

        void bindData(CommodityCirculationNode node, int position) {
            number.setText(String.valueOf(position + 1));
            name.setText(node.getCommodityName());
            amount.setText(amount.getContext().getString(R.string.amount_pattern, node.getAmount()));
            total.setText(total.getContext().getString(R.string.total_cost_pattern, node.getTotalCost()));
        }
    }
}
