package com.takeiteasy.sourceit.e.contacts;

import android.support.annotation.LayoutRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.takeiteasy.sourceit.R;
import com.takeiteasy.sourceit.e.models.Contact;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactsListViewAdapter extends BaseAdapter {

    private final List<Contact> data;

    public ContactsListViewAdapter() {
        data = new ArrayList<>();
    }

    public void updateData(List<Contact> data) {
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    public void addContact(Contact contact) {
        data.add(contact);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Contact getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view;
        ViewHolder viewHolder;

        if (convertView == null) {
            view = inflateView(parent, R.layout.item_list_view_contact);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            view = convertView;
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.bindData(data.get(position));

        return view;
    }

    private View inflateView(ViewGroup parent, @LayoutRes int layoutRes) {
        return LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
    }

    class ViewHolder {
        @BindView(R.id.iv_icon)
        ImageView icon;
        @BindView(R.id.tv_name)
        TextView name;
        @BindView(R.id.tv_email)
        TextView email;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

        void bindData(Contact contact) {
            if (contact.getIcon() != null) {
                icon.setImageURI(contact.getIcon());
            }

            name.setText(contact.getName());
            email.setText(contact.getEmail());
        }
    }
}

