package com.takeiteasy.sourceit.e.contacts;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.takeiteasy.sourceit.R;
import com.takeiteasy.sourceit.base.BaseFragment;
import com.takeiteasy.sourceit.e.models.Contact;
import com.takeiteasy.sourceit.e.utils.DataGenerator;

import butterknife.BindView;

public class ContactsFragment extends BaseFragment implements AdapterView.OnItemClickListener, ContactsRecyclerViewAdapter.OnContactItemClickListener {

    @BindView(R.id.list)
    RecyclerView list;
//    ListView list;

    private ContactsInteractionListener mListener;
//    private ContactsListViewAdapter adapter;
    private ContactsRecyclerViewAdapter adapter;

    public static ContactsFragment newInstance() {

        Bundle args = new Bundle();

        ContactsFragment fragment = new ContactsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_contacts, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.add_contact) {
            addContact();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void addContact() {
//        int number = adapter.getCount() + 1;
        int number = adapter.getItemCount() + 1;
        adapter.addContact(new Contact(
                "Person " + number,
                "email" + number + "@mail.com",
                "some st. " + number,
                null
        ));
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        return bindView(inflater.inflate(R.layout.fragment_contacts_list_view, container, false));
        return bindView(inflater.inflate(R.layout.fragment_contacts_recycler_view, container, false));
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        adapter = new ContactsListViewAdapter();
        adapter = new ContactsRecyclerViewAdapter();
        adapter.updateData(DataGenerator.createContactsList());
        adapter.setListener(this);
        list.setAdapter(adapter);
        list.setLayoutManager(new LinearLayoutManager(getContext()));
        list.addItemDecoration(new DividerItemDecoration(view.getContext(), DividerItemDecoration.VERTICAL));
//        list.setOnItemClickListener(this);
    }

    @Override
    public void onDestroyView() {
//        list.setOnItemClickListener(null);
        adapter.setListener(null);
        super.onDestroyView();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ContactsInteractionListener) {
            mListener = (ContactsInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement ContactsInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//        mListener.showContactDetails(adapter.getItem(position));
    }

    @Override
    public void onItemClicked(Contact contact) {
        mListener.showContactDetails(contact);
    }

    public interface ContactsInteractionListener {
        void showContactDetails(Contact contact);
    }
}
