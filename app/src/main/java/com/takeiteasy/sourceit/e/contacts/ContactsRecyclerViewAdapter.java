package com.takeiteasy.sourceit.e.contacts;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.takeiteasy.sourceit.R;
import com.takeiteasy.sourceit.e.models.Contact;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ContactsRecyclerViewAdapter extends RecyclerView.Adapter<ContactsRecyclerViewAdapter.ViewHolder> {

    private final List<Contact> data;

    private OnContactItemClickListener listener;

    public ContactsRecyclerViewAdapter() {
        data = new ArrayList<>();
    }

    public void setListener(@Nullable OnContactItemClickListener listener) {
        this.listener = listener;
    }

    public void updateData(List<Contact> data) {
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    public void addContact(Contact contact) {
        data.add(contact);
        notifyItemInserted(data.size() - 1);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(inflateView(parent, R.layout.item_recycler_view_contact));
    }

    private View inflateView(ViewGroup parent, @LayoutRes int layoutRes) {
        return LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindData(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public interface OnContactItemClickListener {
        void onItemClicked(Contact contact);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_icon)
        ImageView icon;
        @BindView(R.id.tv_name)
        TextView name;
        @BindView(R.id.tv_email)
        TextView email;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.item)
        void onClick() {
            if (listener != null) {
                listener.onItemClicked(data.get(getAdapterPosition()));
            }
        }

        void bindData(Contact contact) {
            if (contact.getIcon() != null) {
                icon.setImageURI(contact.getIcon());
            }

            name.setText(contact.getName());
            email.setText(contact.getEmail());
        }
    }
}
