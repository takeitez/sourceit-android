package com.takeiteasy.sourceit.e.utils;


import com.takeiteasy.sourceit.e.models.Commodity;
import com.takeiteasy.sourceit.e.models.Contact;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DataGenerator {
    private static final int CONTACTS_LIST_DEF_SIZE = 10;
    private static final int COMODITIES_LIST_DEF_SIZE = 5;

    private DataGenerator() {

    }

    public static List<Contact> createContactsList() {
        List<Contact> list = new ArrayList<>();

        for (int i = 0; i < CONTACTS_LIST_DEF_SIZE; i++) {
            int number = i + 1;
            list.add(new Contact(
                    "Person " + number,
                    "email" + number + "@mail.com",
                    "some st. " + number,
                    null
            ));
        }

        return list;
    }

    public static List<Commodity> createCommoditiesList() {
        List<Commodity> list = new ArrayList<>();
        Random random = new Random();

        for (int i = 0; i < COMODITIES_LIST_DEF_SIZE; i++) {
            int number = i + 1;
            list.add(new Commodity(
                    "Commodity " + number,
                    random.nextInt(300)
            ));
        }

        return list;
    }
}
