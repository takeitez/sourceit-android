package com.takeiteasy.sourceit;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.takeiteasy.sourceit.router.ActivityRouter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements ActivityRouter.OnIntentReadyListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.btn_task_a)
    Button btnTaskA;
    @BindView(R.id.btn_task_b)
    Button btnTaskB;
    @BindView(R.id.btn_task_d)
    Button btnTaskD;
    @BindView(R.id.btn_task_e)
    Button btnTaskE;

    private ActivityRouter activityRouter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        activityRouter = new ActivityRouter(this);

        setSupportActionBar(toolbar);
    }

    @OnClick(R.id.btn_task_a)
    void showTaskA() {
        activityRouter.requestATaskActivityIntent(this);
    }

    @OnClick(R.id.btn_task_b)
    void showTaskB() {
        activityRouter.requestBTaskActivityIntent(this);
    }

    @OnClick(R.id.btn_task_d)
    void showTaskD() {
        activityRouter.requestDTaskActivityIntent(this);
    }

    @OnClick(R.id.btn_task_e)
    void showTaskE() {
        activityRouter.requestETaskActivityIntent(this);
    }

    @Override
    public void onIntentReady(Intent intent) {
        startActivity(intent);
    }
}
