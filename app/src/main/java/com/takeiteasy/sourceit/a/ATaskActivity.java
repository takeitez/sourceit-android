package com.takeiteasy.sourceit.a;

import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.takeiteasy.sourceit.R;
import com.takeiteasy.sourceit.a.authorization.AuthorizationFragment;
import com.takeiteasy.sourceit.a.calculator.CalculatorFragment;
import com.takeiteasy.sourceit.a.textview.HelloPrinterFragment;
import com.takeiteasy.sourceit.a.navigator.ASubTasksNavigationFragment;
import com.takeiteasy.sourceit.a.edittext.TextManipulatorFragment;

public class ATaskActivity extends AppCompatActivity
        implements ASubTasksNavigationFragment.OnSubTaskChosenListener, FragmentManager.OnBackStackChangedListener {

    private Toolbar toolbar;

    @Override
    protected void onStart() {
        super.onStart();
        getSupportFragmentManager().addOnBackStackChangedListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        getSupportFragmentManager().removeOnBackStackChangedListener(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atask);
        setupActionBar();

        setFragment(ASubTasksNavigationFragment.newInstance());
    }

    private void setupActionBar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void setFragment(Fragment fragment) {
        setFragment(fragment, false);
    }

    private void setFragment(Fragment fragment, boolean addToBackStack) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        ft.replace(R.id.container, fragment);

        if (addToBackStack)
            ft.addToBackStack(null);

        ft.commit();
    }

    @Override
    public void showHelloPrinter(@StringRes int titleRes) {
        setFragment(HelloPrinterFragment.newInstance(), true);
        setTitle(titleRes);
    }

    @Override
    public void showTextManipulator(int titleRes) {
        setFragment(TextManipulatorFragment.newInstance(), true);
        setTitle(titleRes);
    }

    @Override
    public void showAuthorization(int titleRes) {
        setFragment(AuthorizationFragment.newInstance(), true);
        setTitle(titleRes);
    }

    @Override
    public void showCalculator(int titleRes) {
        setFragment(CalculatorFragment.newInstance(), true);
        setTitle(titleRes);
    }

    @Override
    public void onBackStackChanged() {
        ActionBar actionBar = getSupportActionBar();

        if (actionBar == null)
            return;

        boolean isBackStackEmpty = getSupportFragmentManager().getBackStackEntryCount() == 0;

        actionBar.setDisplayHomeAsUpEnabled(!isBackStackEmpty);

        if (isBackStackEmpty)
            setTitle(R.string.task_a);
    }
}
