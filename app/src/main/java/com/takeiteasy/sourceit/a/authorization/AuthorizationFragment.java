package com.takeiteasy.sourceit.a.authorization;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.takeiteasy.sourceit.R;

public class AuthorizationFragment extends Fragment implements Credentials.ValidationCallback {

    private EditText etLogin;
    private EditText etEmail;
    private EditText etPhone;
    private EditText etPassword;
    private EditText etConfirmation;
    private TextView tvState;
    private Button btnLogin;

    public AuthorizationFragment() {
        // Required empty public constructor
    }

    public static AuthorizationFragment newInstance() {
        Bundle args = new Bundle();
        AuthorizationFragment fragment = new AuthorizationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_authorization, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
    }

    private void initViews(View view) {
        etLogin = view.findViewById(R.id.et_login);
        etEmail = view.findViewById(R.id.et_email);
        etPhone = view.findViewById(R.id.et_phone);
        etPassword = view.findViewById(R.id.et_password);
        etConfirmation = view.findViewById(R.id.et_confirmation);
        tvState = view.findViewById(R.id.tv_state);
        btnLogin = view.findViewById(R.id.btn_login);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });
    }

    private void login() {
        Credentials credentials = new Credentials(
                readInputValue(etLogin),
                readInputValue(etEmail),
                readInputValue(etPhone),
                readInputValue(etPassword),
                readInputValue(etConfirmation)
        );

        credentials.validate(this);
    }

    private String readInputValue(EditText editText) {
        return editText.getText().toString();
    }

    @Override
    public void validationSuccess() {
        showState(R.string.validation_success);
    }

    @Override
    public void validationFailure(int error) {
        if (error == Credentials.ERROR_MISSING_FIELD) {
           showState(R.string.missing_field);
        } else if (error == Credentials.ERROR_PASSWORDS_MISMATCH) {
            showState(R.string.passwords_mismatch);
        } else if (error == Credentials.ERROR_EMAIL_FORMAT) {
            showState(R.string.wrong_email_format);
        } else if (error == Credentials.ERROR_PHONE_FORMAT) {
            showState(R.string.wrong_phone_format);
        }
    }

    private void showState(@StringRes int stateRes) {
        tvState.setText(stateRes);
    }
}
