package com.takeiteasy.sourceit.a.calculator;

import android.content.Intent;
import android.util.Log;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExpressionProcessor {
    private static final String TAG = ExpressionProcessor.class.getSimpleName();
    private static final String EXPRESSION_FORMAT = "^([0-9]+)([+\\-÷×])([0-9]+)$";
    private static final String OPERATION_ADD = "+";
    private static final String OPERATION_SUB = "-";
    private static final String OPERATION_MUL = "×";
    private static final String OPERATION_DIV = "÷";

    private final String expression;
    private final Pattern expressionPattern;
    private final Matcher expressionMatcher;

    public ExpressionProcessor(String expression) {
        this.expression = expression;
        expressionPattern = Pattern.compile(EXPRESSION_FORMAT);
        expressionMatcher = expressionPattern.matcher(expression);
    }

    public boolean isValidExpression() {
        return expressionMatcher.matches();
    }

    public float compute() throws Exception {
        float result = 0;

        String operation = getOperation();

        switch (operation) {
            case OPERATION_ADD:
                result = add();
                break;
            case OPERATION_SUB:
                result = sub();
                break;
            case OPERATION_MUL:
                result = mul();
                break;
            case OPERATION_DIV:
                result = div();
                break;
        }

        return result;
    }

    private float add() {
        return getFirstArgument() + getSecondArgument();
    }

    private float sub() {
        return getFirstArgument() - getSecondArgument();
    }

    private float mul() {
        return getFirstArgument() * getSecondArgument();
    }

    private float div() throws Exception {
        return getFirstArgument() / getSecondArgument();
    }

    private String getOperation() {
        Log.d(TAG, "getOperation() returned: " + expressionMatcher.group(2));
        return expressionMatcher.group(2);
    }

    private float getFirstArgument() {
        Log.d(TAG, "getFirstArgument() returned: " + expressionMatcher.group(1));
        return Float.valueOf(expressionMatcher.group(1));
    }

    private float getSecondArgument() {
        Log.d(TAG, "getSecondArgument() returned: " + expressionMatcher.group(3));
        return Float.valueOf(expressionMatcher.group(3));
    }
}
