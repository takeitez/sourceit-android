package com.takeiteasy.sourceit.a.textview;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.takeiteasy.sourceit.R;

public class HelloPrinterFragment extends Fragment {

    private TextView tvHelloWorld;
    private Button btnPrint;
    private Button btnClear;

    public HelloPrinterFragment() {
        // Required empty public constructor
    }

    public static HelloPrinterFragment newInstance() {
        HelloPrinterFragment fragment = new HelloPrinterFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_hello_printer, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
    }

    private void initViews(View view) {
        tvHelloWorld = view.findViewById(R.id.tv_hello_world);
        btnPrint = view.findViewById(R.id.btn_print);
        btnClear = view.findViewById(R.id.btn_clear);

        btnPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvHelloWorld.setText(R.string.hello_world);
            }
        });

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvHelloWorld.setText(null);
            }
        });
    }
}
