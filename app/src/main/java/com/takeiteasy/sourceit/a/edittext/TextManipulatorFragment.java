package com.takeiteasy.sourceit.a.edittext;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.takeiteasy.sourceit.R;

import java.util.Deque;
import java.util.LinkedList;

public class TextManipulatorFragment extends Fragment {

    private static final int STACK_SIZE = 5;

    private EditText editText;
    private Button btnConsume;
    private Button btnEmit;

    private Deque<String> stack;

    public TextManipulatorFragment() {
        // Required empty public constructor
    }

    public static TextManipulatorFragment newInstance() {
        Bundle args = new Bundle();
        TextManipulatorFragment fragment = new TextManipulatorFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        stack = new LinkedList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_text_manipulator, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
    }

    private void initViews(View view) {
        editText = view.findViewById(R.id.edit_text);
        btnConsume = view.findViewById(R.id.btn_consume);
        btnEmit = view.findViewById(R.id.btn_emit);

        btnConsume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    consumeString();
                } catch (Exception e) {
                    Toast.makeText(getContext(), e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });

        btnEmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!stack.isEmpty())
                    resetString();
            }
        });
    }

    private boolean isStackFull() {
        return stack.size() == STACK_SIZE;
    }

    private void consumeString() throws Exception {
        if (isStackFull())
            throw new Exception(getString(R.string.stack_full));

        stack.add(editText.getText().toString());
        editText.setText(null);
    }

    private void resetString() {
        editText.setText(stack.removeLast());
    }
}
