package com.takeiteasy.sourceit.a.authorization;

import android.text.TextUtils;

public class Credentials {
    private static final String EMAIL_FORMAT = "^.+@.+\\..+$";
    private static final String PHONE_FORMAT = "^\\+\\d{1,2}\\d{3}\\d{7}$";

    public static final int ERROR_MISSING_FIELD = 0x10;
    public static final int ERROR_PASSWORDS_MISMATCH = 0x11;
    public static final int ERROR_EMAIL_FORMAT = 0x12;
    public static final int ERROR_PHONE_FORMAT = 0x13;

    private final String login;
    private final String email;
    private final String phone;
    private final String password;
    private final String confirmation;

    public Credentials(String login, String email, String phone, String password, String confirmation) {
        this.login = login;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.confirmation = confirmation;
    }

    public void validate(ValidationCallback callback) {
        if (containsEmptyField()) {
            callback.validationFailure(ERROR_MISSING_FIELD);
            return;
        }

        if (!isPasswordsMatch()) {
            callback.validationFailure(ERROR_PASSWORDS_MISMATCH);
            return;
        }

        if (!isEmailValid()) {
            callback.validationFailure(ERROR_EMAIL_FORMAT);
            return;
        }

        if (!isPhoneValid()) {
            callback.validationFailure(ERROR_PHONE_FORMAT);
            return;
        }

        callback.validationSuccess();
    }

    private boolean isPhoneValid() {
        return phone.matches(PHONE_FORMAT);
    }

    private boolean isEmailValid() {

        return email.matches(EMAIL_FORMAT);
    }

    private boolean containsEmptyField() {
        return TextUtils.isEmpty(login)
                || TextUtils.isEmpty(email)
                || TextUtils.isEmpty(phone)
                || TextUtils.isEmpty(password)
                || TextUtils.isEmpty(confirmation);
    }

    private boolean isPasswordsMatch() {
        return password.equals(confirmation);
    }

    public interface ValidationCallback {
        void validationSuccess();
        void validationFailure(int error);
    }
}
