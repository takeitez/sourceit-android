package com.takeiteasy.sourceit.a.navigator;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.takeiteasy.sourceit.R;

public class ASubTasksNavigationFragment extends Fragment {

    private Button btnHelloPrinter;
    private Button btnTextManipulator;
    private Button btnAuthorization;
    private Button btnCalculator;

    private OnSubTaskChosenListener mListener;

    public ASubTasksNavigationFragment() {
        // Required empty public constructor
    }

    public static ASubTasksNavigationFragment newInstance() {
        ASubTasksNavigationFragment fragment = new ASubTasksNavigationFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_asubtasks_navigation, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
    }

    private void initViews(View view) {
        btnHelloPrinter = view.findViewById(R.id.btn_hello_printer);
        btnTextManipulator = view.findViewById(R.id.btn_text_manipulator);
        btnAuthorization = view.findViewById(R.id.btn_authorization);
        btnCalculator = view.findViewById(R.id.btn_calculator);

        btnHelloPrinter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.showHelloPrinter(R.string.hello_printer);
            }
        });

        btnTextManipulator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.showTextManipulator(R.string.text_manipulator);
            }
        });

        btnAuthorization.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.showAuthorization(R.string.authorization);
            }
        });

        btnCalculator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.showCalculator(R.string.calculator);
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnSubTaskChosenListener) {
            mListener = (OnSubTaskChosenListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnSubTaskChosenListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnSubTaskChosenListener {
        void showHelloPrinter(@StringRes int titleRes);
        void showTextManipulator(@StringRes int titleRes);
        void showAuthorization(@StringRes int titleRes);
        void showCalculator(@StringRes int titleRes);
    }
}
