package com.takeiteasy.sourceit.a.calculator;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.takeiteasy.sourceit.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CalculatorFragment extends Fragment {

    private EditText etInput;
    private TextView tvResult;
    private Button btnCalculate;

    public CalculatorFragment() {
        // Required empty public constructor
    }

    public static CalculatorFragment newInstance() {
        Bundle args = new Bundle();
        CalculatorFragment fragment = new CalculatorFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_calculator, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
    }

    private void initViews(View view) {
        etInput = view.findViewById(R.id.et_input);
        tvResult = view.findViewById(R.id.tv_result);
        btnCalculate = view.findViewById(R.id.btn_calculate);

        btnCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calculate();
            }
        });
    }

    private void calculate() {
        ExpressionProcessor processor = new ExpressionProcessor(etInput.getText().toString());

        if (!processor.isValidExpression()) {
            tvResult.setText(R.string.invalid_expression);
            return;
        }

        try {
            tvResult.setText(getString(R.string.result_pattern, processor.compute()));
        } catch (Exception e) {
            tvResult.setText(e.getLocalizedMessage());
        }
    }
}
