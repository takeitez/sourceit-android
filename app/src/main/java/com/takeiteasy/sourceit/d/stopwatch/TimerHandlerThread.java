package com.takeiteasy.sourceit.d.stopwatch;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;

public class TimerHandlerThread extends HandlerThread {
    private static final String THREAD_TIMER = TimerHandlerThread.class.getSimpleName();
    private static final int INITIAL_VALUE = 30;
    private static final int TICK_DELAY_IN_MILLIS = 1_000;
    private static final int CMD_START = 0x10;

    private Handler backgroundHandler;
    private Handler uiHandler;
    private OnTimeUpdatedListener listener;
    private int value;
    private boolean isRunning;

    private Runnable timeUpdateRunnable = new Runnable() {
        @Override
        public void run() {
            if (listener != null) {
                listener.onTimeUpdated(value);
            }
        }
    };

    public TimerHandlerThread(Handler handler, OnTimeUpdatedListener listener) {
        super(THREAD_TIMER);
        this.value = INITIAL_VALUE;
        this.uiHandler = handler;
        this.listener = listener;

        uiHandler.post(timeUpdateRunnable);
    }

    @Override
    protected void onLooperPrepared() {
        backgroundHandler = new Handler(getLooper(), new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                int cmd = msg.what;

                if (cmd == CMD_START) {
                    uiHandler.post(timeUpdateRunnable);
                    value--;

                    if (value > 0) {
                        tick();
                    }
                }

                return true;
            }
        });
    }

    private void tick() {
        backgroundHandler.sendEmptyMessageDelayed(CMD_START, TICK_DELAY_IN_MILLIS);
    }

    public void startTimer() {
        isRunning = true;
        backgroundHandler.sendEmptyMessage(CMD_START);
    }

    public void stopTimer() {
        isRunning = false;
        backgroundHandler.removeMessages(CMD_START);
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void resetTimer() {
        isRunning = false;
        value = INITIAL_VALUE;
        uiHandler.post(timeUpdateRunnable);
        backgroundHandler.removeMessages(CMD_START);
    }

    public interface OnTimeUpdatedListener {
        void onTimeUpdated(int time);
    }
}
