package com.takeiteasy.sourceit.d;

import android.content.res.Configuration;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.takeiteasy.sourceit.R;
import com.takeiteasy.sourceit.d.stopwatch.TimerFragment;
import com.takeiteasy.sourceit.d.tabata.TabataTimerFragment;
import com.takeiteasy.sourceit.d.timer.StopwatchFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DTaskActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener {

    private static final String ARG_LAST_SELECTED_TAB = "ARG_LAST_SELECTED_TAB";
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_d_task);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        setTitle(R.string.task_d);
        tabLayout.addOnTabSelectedListener(this);
        if (savedInstanceState == null)
            setFragment(TimerFragment.newInstance());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        tabLayout.removeOnTabSelectedListener(this);
    }

    private void setFragment(Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        int position = tab.getPosition();
        if (position == 0) {
            setFragment(TimerFragment.newInstance());
        } else if (position == 1) {
            setFragment(StopwatchFragment.newInstance());
        } else if (position == 2) {
            setFragment(TabataTimerFragment.newInstance());
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
