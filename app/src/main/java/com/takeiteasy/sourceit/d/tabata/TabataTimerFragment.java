package com.takeiteasy.sourceit.d.tabata;


import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.takeiteasy.sourceit.R;
import com.takeiteasy.sourceit.base.BaseFragment;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnLongClick;

public class TabataTimerFragment extends BaseFragment implements TabataTimerHandlerThread.OnTimeUpdatedListener {

    @BindView(R.id.tv_time)
    TextView tvTime;

    private TabataTimerHandlerThread thread;

    public TabataTimerFragment() {
        // Required empty public constructor
    }

    public static TabataTimerFragment newInstance() {
        
        Bundle args = new Bundle();
        
        TabataTimerFragment fragment = new TabataTimerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return bindView(inflater.inflate(R.layout.fragment_tabata_timer, container, false));
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        thread = new TabataTimerHandlerThread(new Handler(), this);
        thread.start();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        thread.quit();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @OnClick(R.id.tv_time)
    void onTimerClicked() {
        if (thread.isRunning())
            thread.stopTimer();
        else
            thread.startTimer();
    }

    @OnLongClick(R.id.tv_time)
    boolean onTimerLongClick() {
        thread.resetTimer();
        return true;
    }

    @Override
    public void onWorkTimeUpdated(int time) {
        tvTime.setTextColor(Color.GREEN);
        tvTime.setText(String.valueOf(time));
    }

    @Override
    public void onRestTimeUpdated(int time) {
        tvTime.setTextColor(Color.RED);
        tvTime.setText(String.valueOf(time));
    }
}
