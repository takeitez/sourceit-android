package com.takeiteasy.sourceit.d.tabata;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;

public class TabataTimerHandlerThread extends HandlerThread {
    private static final String THREAD_TABATA_TIMER = TabataTimerHandlerThread.class.getSimpleName();
    private static final int INITIAL_WORK_VALUE = 10;
    private static final int INITIAL_REST_VALUE = 5;
    private static final int TICK_DELAY_IN_MILLIS = 1_000;
    private static final int CMD_START_WORK = 0x10;
    private static final int CMD_START_REST = 0x11;

    private Handler backgroundHandler;
    private Handler uiHandler;
    private OnTimeUpdatedListener listener;
    private int workValue;
    private int restValue;
    private boolean isRunning;
    private boolean isWork;

    private Runnable timeUpdateRunnable = new Runnable() {
        @Override
        public void run() {
            if (listener != null) {
                if (isWork)
                    listener.onWorkTimeUpdated(workValue);
                else
                    listener.onRestTimeUpdated(restValue);
            }
        }
    };

    public TabataTimerHandlerThread(Handler handler, OnTimeUpdatedListener listener) {
        super(THREAD_TABATA_TIMER);
        this.workValue = INITIAL_WORK_VALUE;
        this.restValue = INITIAL_REST_VALUE;
        this.isWork = true;
        this.uiHandler = handler;
        this.listener = listener;

        uiHandler.post(timeUpdateRunnable);
    }

    @Override
    protected void onLooperPrepared() {
        backgroundHandler = new Handler(getLooper(), new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                int cmd = msg.what;

                if (cmd == CMD_START_WORK) {
                    uiHandler.post(timeUpdateRunnable);
                    handleWorkCommand();
                } else if (cmd == CMD_START_REST) {
                    uiHandler.post(timeUpdateRunnable);
                    handleRestCommand();
                }

                return true;
            }
        });
    }

    private void handleWorkCommand() {
        workValue--;

        tick(workValue > 0);

        if (workValue == 0) {
            workValue = INITIAL_WORK_VALUE;
        }
    }

    private void handleRestCommand() {
        restValue--;

        tick(restValue < 1);

        if (restValue < 1) {
            restValue = INITIAL_REST_VALUE;
        }
    }

    private void tick(boolean isWork) {
        this.isWork = isWork;
        backgroundHandler.sendEmptyMessageDelayed(isWork ? CMD_START_WORK : CMD_START_REST, TICK_DELAY_IN_MILLIS);
    }

    public void startTimer() {
        isRunning = true;
        backgroundHandler.sendEmptyMessage(isWork ? CMD_START_WORK : CMD_START_REST);
    }

    public void stopTimer() {
        isRunning = false;
        backgroundHandler.removeMessages(isWork ? CMD_START_WORK : CMD_START_REST);
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void resetTimer() {
        isRunning = false;
        isWork = true;
        workValue = INITIAL_WORK_VALUE;
        restValue = INITIAL_REST_VALUE;
        uiHandler.post(timeUpdateRunnable);
        backgroundHandler.removeMessages(CMD_START_WORK);
        backgroundHandler.removeMessages(CMD_START_REST);
    }

    public interface OnTimeUpdatedListener {
        void onWorkTimeUpdated(int time);
        void onRestTimeUpdated(int time);
    }
}
