package com.takeiteasy.sourceit.d.timer;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;

import com.takeiteasy.sourceit.d.stopwatch.TimerHandlerThread;

public class StopwatchHandlerThread extends HandlerThread {
    private static final String THREAD_STOPWATCH = StopwatchHandlerThread.class.getSimpleName();
    private static final int INITIAL_VALUE = 0;
    private static final int TICK_DELAY_IN_MILLIS = 1_000;
    private static final int CMD_START = 0x10;

    private Handler backgroundHandler;
    private Handler uiHandler;
    private OnTimeUpdatedListener listener;
    private int value;
    private boolean isRunning;

    private Runnable timeUpdateRunnable = new Runnable() {
        @Override
        public void run() {
            if (listener != null) {
                listener.onTimeUpdated(value);
            }
        }
    };

    public StopwatchHandlerThread(Handler handler, OnTimeUpdatedListener listener) {
        super(THREAD_STOPWATCH);
        this.value = INITIAL_VALUE;
        this.uiHandler = handler;
        this.listener = listener;

        uiHandler.post(timeUpdateRunnable);
    }

    @Override
    protected void onLooperPrepared() {
        backgroundHandler = new Handler(getLooper(), new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                int cmd = msg.what;

                if (cmd == CMD_START) {
                    uiHandler.post(timeUpdateRunnable);
                    value++;

                    tick();
                }

                return true;
            }
        });
    }

    private void tick() {
        backgroundHandler.sendEmptyMessageDelayed(CMD_START, TICK_DELAY_IN_MILLIS);
    }

    public void startTimer() {
        isRunning = true;
        backgroundHandler.sendEmptyMessage(CMD_START);
    }

    public void stopTimer() {
        isRunning = false;
        backgroundHandler.removeMessages(CMD_START);
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void resetTimer() {
        isRunning = false;
        value = INITIAL_VALUE;
        uiHandler.post(timeUpdateRunnable);
        backgroundHandler.removeMessages(CMD_START);
    }

    public interface OnTimeUpdatedListener {
        void onTimeUpdated(int time);
    }
}
