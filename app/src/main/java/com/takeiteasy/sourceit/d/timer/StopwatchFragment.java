package com.takeiteasy.sourceit.d.timer;


import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.takeiteasy.sourceit.R;
import com.takeiteasy.sourceit.base.BaseFragment;
import com.takeiteasy.sourceit.d.stopwatch.TimerHandlerThread;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnLongClick;

public class StopwatchFragment extends BaseFragment implements StopwatchHandlerThread.OnTimeUpdatedListener {

    @BindView(R.id.tv_time)
    TextView tvTime;

    private StopwatchHandlerThread thread;

    public StopwatchFragment() {
        // Required empty public constructor
    }

    public static StopwatchFragment newInstance() {
        
        Bundle args = new Bundle();
        
        StopwatchFragment fragment = new StopwatchFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return bindView(inflater.inflate(R.layout.fragment_stopwatch, container, false));
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        thread = new StopwatchHandlerThread(new Handler(), this);
        thread.start();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        thread.quit();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @OnClick(R.id.tv_time)
    void onTimerClicked() {
        if (thread.isRunning())
            thread.stopTimer();
        else
            thread.startTimer();
    }

    @OnLongClick(R.id.tv_time)
    boolean onTimerLongClick() {
        thread.resetTimer();
        return true;
    }

    @Override
    public void onTimeUpdated(int time) {
        tvTime.setText(String.valueOf(time));
    }
}
