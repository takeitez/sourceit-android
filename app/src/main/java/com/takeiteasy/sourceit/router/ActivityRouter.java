package com.takeiteasy.sourceit.router;

import android.content.Context;
import android.content.Intent;

import com.takeiteasy.sourceit.MainActivity;
import com.takeiteasy.sourceit.a.ATaskActivity;
import com.takeiteasy.sourceit.b.BTaskActivity;
import com.takeiteasy.sourceit.d.DTaskActivity;
import com.takeiteasy.sourceit.e.ETaskActivity;

import java.lang.ref.WeakReference;

public class ActivityRouter {
    private final WeakReference<Context> contextReference;

    public ActivityRouter(Context context) {
        contextReference = new WeakReference<>(context);
    }

    public void requestATaskActivityIntent(OnIntentReadyListener listener) {
        Intent intent = new Intent(contextReference.get(), ATaskActivity.class);
        listener.onIntentReady(intent);
    }

    public void requestBTaskActivityIntent(OnIntentReadyListener listener) {
        Intent intent = new Intent(contextReference.get(), BTaskActivity.class);
        listener.onIntentReady(intent);
    }

    public void requestDTaskActivityIntent(OnIntentReadyListener listener) {
        Intent intent = new Intent(contextReference.get(), DTaskActivity.class);
        listener.onIntentReady(intent);
    }

    public void requestETaskActivityIntent(OnIntentReadyListener listener) {
        Intent intent = new Intent(contextReference.get(), ETaskActivity.class);
        listener.onIntentReady(intent);
    }

    public interface OnIntentReadyListener {
        void onIntentReady(Intent intent);
    }
}
